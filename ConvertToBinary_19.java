//19. Write a Java program to convert a decimal number to binary number.

import java.util.Scanner;

public class ConvertToBinary_19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a Decimal Number: ");
        int binary = Integer.parseInt(scanner.nextLine());

        System.out.println("Binary number is: " + Integer.toBinaryString(binary));
    }
}
