//34. Write a Java program to compute the area of a hexagon.

import sun.plugin2.util.SystemUtil;

import java.util.Scanner;

public class HexagonArea_34 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input the length of a side of the hexagon: ");
        double side = scanner.nextDouble();

        double area = (6 * (side*side)) / (4 * Math.tan (Math.PI/6));  //(6 * s^2)/(4*tan(π/6))
        System.out.printf("\nThe area of the hexagon is: %.14f", area);
    }
}
