//7. Write a Java program that takes a number as input and prints its multiplication table upto 10.

import java.util.Scanner;

public class MultiplicationTable_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a number: ");
        int number = scanner.nextInt();
        System.out.println("\n");

        for (int i = 1; i <= 10; i++){
            System.out.println(number + " x " + (i) + " = " + (number*i));
        }
    }

}
