//47. Write a Java program to display the current date time in specific format.

import java.text.SimpleDateFormat;
import java.util.Date;

public class SpecificFormat_47 {
    public static void main(String[] args) {
        SimpleDateFormat dFormat = new SimpleDateFormat("YYYY/MM/DD HH:mm:ss.mmm");
        Date date = new Date();
        System.out.println("Now: " + dFormat.format(date));

    }
}
