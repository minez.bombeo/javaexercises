//20. Write a Java program to convert a decimal number to hexadecimal number.

import java.util.Scanner;

public class ConvertToHex_20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a Decimal Number: ");
        int hex = Integer.parseInt(scanner.nextLine());

        System.out.println("Binary number is: " + Integer.toHexString(hex).toUpperCase());
    }
}
