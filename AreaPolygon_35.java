//35. Write a Java program to compute the area of a polygon.

import java.util.Scanner;

public class AreaPolygon_35 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input the number of sides on the polygon: ");
        int nSide = scanner.nextInt();
        System.out.print("Input the length of one of the sides: ");
        double length = scanner.nextInt();

        double area = (nSide * (length*length)) / (4 * Math.tan(Math.PI/nSide));//(n*s^2)/(4*tan(π/n))
        System.out.printf("\nThe area is: %.14f", area);
    }
}
