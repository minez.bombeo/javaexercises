//26. Write a Java program to convert a octal number to a binary number.

import java.util.Scanner;

public class OctToBin_26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input an octal number : ");
        int oct = Integer.parseInt(scanner.nextLine(), 8);

        System.out.println("Equivalent binary number: " + Integer.toString(oct, 2));
    }
}
