//28. Write a Java program to convert a hexadecimal to a decimal number.

import java.util.Scanner;

public class HexToDec_28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a hexadecimal number: ");
        int oct = Integer.parseInt(scanner.nextLine(), 8);

        System.out.println("Equivalent decimal number: " + Integer.toHexString(oct));
    }
}
