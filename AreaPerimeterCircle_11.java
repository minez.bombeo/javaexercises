//11. Write a Java program to print the area and perimeter of a circle.

import java.util.Scanner;

class AreaPerimeterCircle_11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the radius: ");
        double radius = scanner.nextDouble();

        double perimeter = 3.141592653589793 * 2 * radius;
        double area = 3.141592653589793 * (radius * radius);
        System.out.println("Perimeter is = " + perimeter);
        System.out.println("Area is = " + area);
    }
}
