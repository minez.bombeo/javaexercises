//39. Write a Java program to create and display unique three-digit number using 1, 2, 3, 4. Also count how many three-digit numbers are there.

import java.util.Scanner;

public class UniqueDigits_39 {
    public static void main(String[] args) {

        int count = 0;
        for (int n1 = 1; n1 <= 4; n1++) {
            for (int n2 = 1; n2 <= 4; n2++) {
                for (int n3 = 1; n3 <= 4; n3++) {
                    if (n1 != n2 && n1 != n3 && n2 != n3) {
                        System.out.println(n1 + "" + n2 + "" + n3);
                        count++;
                    }
                }
            }
        }
        System.out.println("\nTotal number of the three-digit-number is " + count);
    }
}
