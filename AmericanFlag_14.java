/************************************************
14. Write a Java program to print an American
flag on the screen. Go to the editor
Expected Output

* * * * * * ==================================
 * * * * *  ==================================
* * * * * * ==================================
 * * * * *  ==================================
* * * * * * ==================================
 * * * * *  ==================================
* * * * * * ==================================
 * * * * *  ==================================
* * * * * * ==================================
==============================================
==============================================
==============================================
==============================================
==============================================
==============================================
 ************************************************/

public class AmericanFlag_14 {
    public static void main(String[] args) {
        String pat1 = "* * * * * * ==================================";
        String pat2 = " * * * * *  ==================================";
        String pat3 = "==============================================";

        System.out.println(pat1);
        System.out.println(pat2);
        System.out.println(pat1);
        System.out.println(pat2);
        System.out.println(pat1);
        System.out.println(pat2);
        System.out.println(pat1);
        System.out.println(pat2);
        System.out.println(pat1);
        System.out.println(pat3);
        System.out.println(pat3);
        System.out.println(pat3);
        System.out.println(pat3);
        System.out.println(pat3);
        System.out.println(pat3);
    }
}
