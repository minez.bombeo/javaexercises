//17. Write a Java program to add two binary numbers.

import java.util.Scanner;

public class AddBinary_17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input first binary number: ");
        int firstBin = Integer.parseInt(scanner.nextLine(), 2);

        System.out.println("Input secondary binary number: ");
        int secondBin = Integer.parseInt(scanner.nextLine(), 2);

        int addBin = firstBin + secondBin;

        System.out.println("Sum of two binary numbers: " + Integer.toBinaryString(addBin));

    }
}
