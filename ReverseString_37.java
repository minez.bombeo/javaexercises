//37. Write a Java program to reverse a string.

import java.util.Scanner;

public class ReverseString_37 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input a string: ");
        char[] reverse = scanner.nextLine().toCharArray();

        System.out.print("Reverse string: ");
        for (int i = reverse.length -1; i >= 0; i--){
            System.out.print(reverse[i]);
        }
    }
}
