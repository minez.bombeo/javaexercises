//45. Write a Java program to find the size of a specified file.

import java.io.File;

public class FileSize_45 {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Kasutaja\\Desktop\\ReadMe.txt");
        File file2 = new File("C:\\Users\\Kasutaja\\Desktop\\CV\\CV_template.docx");

        System.out.print("C:\\Users\\Kasutaja\\Desktop\\ReadMe.txt : " + file.length() + " bytes\n");
        System.out.print("C:\\Users\\Kasutaja\\Desktop\\CV\\CV_template.docx : "+ file2.length() + " bytes");
    }

}
