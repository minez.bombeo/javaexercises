/****************************************************************************
 * Write a Java program to display the following pattern. Go to the editor  *
 * Sample Pattern :                                                         *
 *                                                                          *
 *    J    a   v     v  a                                                   *
 *    J   a a   v   v  a a                                                  *
 * J  J  aaaaa   V V  aaaaa                                                 *
 *  JJ  a     a   V  a     a                                                *
 *                                                                          *
 *  By: Minez Bombeo, 27042020                                              *
 ****************************************************************************/

public class Pattern_8 {
    String patternJ1 = "   J";
    String patternJ2 = "J  J";
    String patternJ3 = " JJ ";
    String patternA1 = "   a  ";
    String patternA2 = "  a a ";
    String patternA3 = " aaaaa ";
    String patternA4 = "a     a";
    String patternV1 = "v     v";
    String patternV2 = " v   v ";
    String patternV3 = "  V V  ";
    String patternV4 = "   V   ";

    private void JavaPattern(){
        System.out.println(patternJ1 + " " + patternA1 + " " + patternV1 + patternA1);
        System.out.println(patternJ1 + " " + patternA2 + " " + patternV2 + patternA2);
        System.out.println(patternJ2 + " " + patternA3 + "" + patternV3 + patternA3);
        System.out.println(patternJ3 + " " + patternA4 + "" + patternV4 + patternA4);
    }

    public static void main(String[] args) {
        Pattern_8 pattern = new Pattern_8();
            pattern.JavaPattern();
    }
}
