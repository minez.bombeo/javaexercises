//44. Write a Java program that accepts an integer (n) and computes the value of n+nn+nnn

import java.util.Scanner;

public class ComputeInt_44 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input number: ");
        int num = scanner.nextInt();
        int sum = num + (num*11) + (num*111);

        System.out.printf("%d + %d%d + %d%d%d = %d", num, num, num, num, num, num, sum );

    }
}
