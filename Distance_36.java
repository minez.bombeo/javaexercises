/*36. Write a Java program to compute the distance between two points on the surface of earth.
Input Data:
Input the latitude of coordinate 1: 25
Input the longitude of coordinate 1: 35
Input the latitude of coordinate 2: 35.5
Input the longitude of coordinate 2: 25.5
*/

import java.util.Scanner;

public class Distance_36 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Input the latitude of coordinate 1: ");
        double lat1 = Math.toRadians(scanner.nextDouble());

        System.out.printf("Input the longitude of coordinate 1: ");
        double lon1 = Math.toRadians(scanner.nextDouble());

        System.out.printf("Input the latitude of coordinate 2: ");
        double lat2 = Math.toRadians(scanner.nextDouble());

        System.out.printf("Input the longitude of coordinate 2: ");
        double lon2 = Math.toRadians(scanner.nextDouble());

        double radius = 6371.01;
        double distance = radius * Math.acos(Math.sin(lat1)*Math.sin(lat2) + Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon1 - lon2));

        //d = radius * arccos(sin(x1) * sin(x2) + cos(x1) * cos(x2) * cos(y1 - y2))

        System.out.printf("The distance between those points is: %.14f km", distance);
    }
}
