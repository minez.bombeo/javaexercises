//41. Write a Java program to print the ascii value of a given character.

public class Ascii_41 {
    public static void main(String[] args) {

        char in = 'Z';
        int ascii = (int)in;

        System.out.printf("The ASCII value of %c is :%d", in, ascii);
    }

}
