//31. Write a Java program to check whether Java is installed on your computer

public class JavaInstalled_31 {
    private static Object JavaInstalled_31;

    public static void main(String[] args) {
        String version = System.getProperty("java.version");
        String runtime = System.getProperty("java.runtime.version");
        String home = System.getProperty("java.home");
        String vendor = System.getProperty("java.vendor");
        String url = System.getProperty("java.vendor.url");
        String classPath = System.getProperty("java.class.path");

        System.out.println("Java Version: " + version);
        System.out.println("Java Runtime Version: " + runtime);
        System.out.println("Java Home: " + home);
        System.out.println("Java Vendor: " + vendor);
        System.out.println("Java Vendor URL: " + url);
        System.out.println("Java Class Path: " + classPath);
    }
}
