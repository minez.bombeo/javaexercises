//33. Write a Java program and compute the sum of the digits of an integer.

import java.util.Scanner;

public class SumInteger_33 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input an integer: ");
        int input = scanner.nextInt();

        int sum = 0;
        while (input != 0) {
            sum = sum + (input % 10);
            input = input / 10;
        }
        System.out.printf("The sum of the digits is: %d", sum);
    }
}

