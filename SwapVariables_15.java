//15. Write a Java program to swap two variables.

public class SwapVariables_15 {
    public static void main(String[] args) {
        String firstVar = "First value";
        String secondVar = "Second value";
        String thirdVar = "";

        thirdVar = firstVar;
        firstVar = secondVar;
        secondVar = thirdVar;

        System.out.println("First variable is : " + firstVar);
        System.out.println("Second variable is: " + secondVar);
    }

}
